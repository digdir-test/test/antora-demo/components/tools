= Git and Gitlab command line 

== Prerequisites (Windows 10)
=== Gitbash shell installed
Download and install Gitbash App from  https://gitforwindows.org/

=== Open Gitbash shell
Start Gitbash App

=== Verify your git installation
git --version


=== Verify git user  

    git config --global user.name


=== Verify git email  

    git config --global user.email


=== Install jq Json command line processor
Run Powershell as administrator and install as follows:

    PS C:\WINDOWS\system32>chocolatey install jq

Ref. https://stedolan.github.io/jq/download/

=== Install curl
See https://curl.haxx.se/windows/

Also see https://develop.zendesk.com/hc/en-us/articles/360001068567-Installing-and-using-cURL

    Essence: Download curl and copy the three binaries to c:/curl, then add c:/curl to the system path. 

=== git SSH key pair (move from here to general git setup)
Ref. https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair

    ssh-keygen -t ed25519 -C "erik.hagen@digdir.no"

=== Personal access token
Add a personal access token, if you don't have one already:

See https://gitlab.com/profile/personal_access_tokens

You will need this for API commands starting with 

    _curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72"_

== Selected git commands
Ref. https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html



    cd /c/gitlab
    mkdir erik-hagen
    cd /c/gitlab/erik-hagen
    mkdir nsb
    cd /c/gitlab/erik-hagen/nsb
    mkdir common
    mkdir components

    mkdir components/architecture-landscape
    

    //
    // Script starting here
    //
    
    // PRIVATE_TOKEN="Wu2zJXeohr1ZYeCG5M72"
    PRIVATE_TOKEN="F6nUq1nvsCJUVpRYe9L3"

    ROOT_GROUP_NAME="NER Norway"
    ROOT_GROUP_PATH="ner-norway"
    ROOT_GROUP_ID="6835396"
    
    cd /c/gitlab/
    mkdir $ROOT_GROUP_PATH
    cd /c/gitlab/$ROOT_GROUP_PATH

    GROUPNAME="Components"
    GROUPPATH="components"
    DESCRIPTION="Antora%20components of " $ROOT_GROUP_NAME

    mkdir $GROUPPATH

    //create the gitlab group
    curl -s --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" -X POST "https://gitlab.com/api/v4/groups?name=$GROUPNAME&path=$GROUPPATH&parent_id=$ROOT_GROUP_ID&visibility=public&lfs_enabled=true&description=$DESCRIPTION" | jq >group_$GROUPPATH.txt

    // find the new group id     
    GROUP_ID=$(grep -w "id" group_$GROUPPATH.txt | grep -Eo "[[:digit:]]+")
    echo "ID of new group ($GROUPNAME): " $GROUP_ID

    // Delete the new group
    //curl -s --header "PRIVATE-TOKEN: $PRIVATE_TOKEN"  -X DELETE "https://gitlab.com/api/v4visibi&path=lity=public/groups/$GROUP_ID" | jq
    
    //
    //
    //
    PRIVATE_TOKEN="F6nUq1nvsCJUVpRYe9L3"
    ROOT_GROUP_NAME="NER%20Norway"
    ROOT_GROUP_PATH="ner-norway"
    ROOT_GROUP_ID="6835396"
    
    GROUPPATH=ROOT_GROUP_PATH
    PROJECTNAME="Common"
    PROJECTPATH="common"
    DESCRIPTION="Common%20files"

    #create top level project common
    curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" -X POST "https://gitlab.com/api/v4/projects?name=$PROJECTNAME&path=$PROJECTPATH&description=$DESCRIPTION&visibility=public" | jq >project_$GROUPPATH_$PROJECTPATH.txt


curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" -X POST "https://gitlab.com/api/v4/projects?name=$PROJECTNAME&path=$PROJECTPATH&visibility=public" | jq
    

== Gitlab api

=== List all my groups

. With private token for authentication:

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" https://gitlab.com/api/v4/groups | jq


. With username/password  for authentication:

    curl -u {erik.hagen@digdir.no}:{#7Wonders} https://gitlab.com/api/v4/groups | jq

. No authentication:

    curl https://gitlab.com/api/v4/groups | jq
    

. Order and sort by path

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups?order_by=path&sort=asc" | jq | grep path

. Sort by name:

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" https://gitlab.com/api/v4/groups?sort=asc | jq | grep name

. Sort by web_url using linux sort and grep: 

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups" | jq | sort | grep -i "web_url"
    

    
. Output to file and filter by grep (1 line before and 2 after each grep match):

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups" | jq > groups.txt

    grep "/erik-hagen" groups.txt -B1 -A2

=== List subgroups of group with given id

    curl https://gitlab.com/api/v4/groups/6815654/subgroups | jq | grep name_with_namespace

=== List details of given group

. With projects (default):

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups/6815605" | jq

. Without projects:

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups/6815605?with_projects=false" | jq



=== List projects of group with given id

    curl https://gitlab.com/api/v4/groups/6815655/projects?sort=asc | jq | grep name_with_namespace
    
    
With subgroups:

    curl "https://gitlab.com/api/v4/groups/6815605/projects?include_subgroups=true&sort=asc" | jq | grep name_with_namespace
    


=== Create a new subgroup


WARNING: TODO: Add visibility=private / internal / public and lfs_enabled... ref. https://docs.gitlab.com/ee/api/groups.html

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" -X POST "https://gitlab.com/api/v4/groups?name=Testgroup1&path=testgroup1&parent_id=6831516" | jq


    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" -X POST "https://gitlab.com/api/v4/groups?name=Testgroup2&path=testgroup2&parent_id=6834958" | jq
    
    curl -s --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" -X POST "https://gitlab.com/api/v4/groups?name=Testgroup2&path=testgroup2&parent_id=6834958&visibility=private&lfs_enabled=true" | jq

=== Update a group

NOTE: Assuming group with id=6834958 exists

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" -X PUT "https://gitlab.com/api/v4/groups/6834958?name=Testgroup%201&path=testgroup-1" | jq
    
=== Delete a group

    curl -s --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72"  -X DELETE "https://gitlab.com/api/v4/groups/6835117" | jq


=== Search for group

NOTE: Searching name and path only (not web_url).

    curl --header "PRIVATE-TOKEN: Wu2zJXeohr1ZYeCG5M72" "https://gitlab.com/api/v4/groups?search=erik-hagen" | jq